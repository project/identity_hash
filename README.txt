********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Identity Hash Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Identifies user based on hash string.

A hash looks something  like this 'g74sd9hy3',  and can be  used  to
identify a user without using their username password or uid.
The hash could be  used in  a link included in an email or RSS feed,
so that when the link is clicked, Drupal  will  know the identity of
the user.

This module provides an API, don't install it unless required by another module.



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire directory of this module into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.
  



********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/identity_hash
   
- Comission New Features:
   http://www.codepositive.com/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

        
********************************************************************
ACKNOWLEDGEMENT

Developed by Robert Castelo for Code Positive <http://www.codepositive.com>








